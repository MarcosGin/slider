<?php
	require_once("../models/class.slider.php");
    require_once("../models/class.image.php");

	$title = $_POST['title'];
	$description = $_POST['description'];

	$objImage = new Image();
	$objImage->newImage($_FILES['image']);
    $objImage->newImage($_FILES['image'], 500);

	$images = $objImage->getImage();

	$slider_i = new Slider();
	$slider_i->setAttributes($title, $description, $images[0], $images[1]);

	if($slider_i->insert()){
	    header('location: ../index.php?m=2');
    }
?>