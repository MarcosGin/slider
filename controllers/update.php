<?php
	require_once("../models/class.slider.php");
	$slider_u = new Slider();
	if(isset($_GET['id'])){
		$consulta = $slider_u->select_id($_GET['id']);

		if ($slider_u->consulta->num_rows > 0) {
			while ($registro = $slider_u->consulta->fetch_assoc()) {
			include("../views/form.update.php");
			}
		}
	}

	if (isset($_POST['update'])) {
		$id = $_POST['id'];
		$title = $_POST['title'];
		$description = $_POST['description'];
		$slider_u->setAttributes($title, $description, $id);

			if ($slider_u->update()) {
				header("location:../index.php?m=6");
			}
		}
	
?>