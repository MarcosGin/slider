<?php
	require_once("../models/class.slider.php");
	require_once("../models/class.image.php");

	if (isset($_GET['id'])) {
		$slider_d = new Slider();
		$consulta = $slider_d->select_id($_GET['id']);

		if ($slider_d->consulta->num_rows > 0) {
			$registro = $slider_d->consulta->fetch_assoc();

			$objImg = new Image();
			$objImg->deleteImg($registro['image']);
			$objImg->deleteImg($registro['image_port']);

			$m = $slider_d->delete($_GET['id']);
			if ($m == 4) {
				header("location: ../index.php?m=$m");
			}
			
		}

		
	}
?>