<?php
	Abstract class Connection extends mysqli {
		private $host = 'localhost';
		private $user = 'root';
		private $password = '';
		private $db = 'slider';

		public function __construct(){
			parent::__construct($this->host, $this->user, $this->password, $this->db);
			$this->set_charset('utf8');
		}

		public function __destruct(){
			$this->close();
		}
	}
?>