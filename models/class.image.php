<?php

    class Image{
        private $path = 'views/images/'; //Ruta de defecto donde se guarda la imagen
        private $images = []; //Variable que guarda las rutas de las imagenes creadas.

        //Params
        // param $image = Variable FILES de la imagen -> Puede ser un vector con muchas imagenes
        // param $newMax = Sobreescribe el tamaño de la imagen por defecto de la imagen (1000)
        // param $newPath = Sobreescribe la ruta donde se va guardar la imagenes

        public function newImage($image, $newMax= null, $newPath = null){
            if($newPath){
                $this->path = $newPath;
            }
            $max = $newMax ? $newMax : 1000;

            foreach ($image['error'] as $key => $value) {
                $info = getimagesize($image['tmp_name'][$key]);//Obtiene en un vector los datos de la imagen
                $img = "";
                $ext= pathinfo($image['name'][$key]);
                $ext = $ext["extension"];//Obtenemos la extensión en string de la imagen.
                $name= uniqid();//Generamos un nombre unico de la imagen.
                $path = $this->path . $name . '.' . $ext;//Ruta de la imagen 
                $error = false;
                switch ($info[2]) {
                    case 1:
                        $img = imagecreatefromgif($image['tmp_name'][$key]); break;
                    case 2:
                        $img = imagecreatefromjpeg($image['tmp_name'][$key]); break;
                    case 3:
                        $img = imagecreatefrompng($image['tmp_name'][$key]); break;
                    default:
                        $error = true;
                }
                if(!$error){
                    $original = $img;
                    $original_w = imagesx($original);//Obtenemos el width
                    $original_h = imagesy($original);//Obtenemos el height

                    if($original_w>$original_h) {
                        $muestra_w = $max;
                        $muestra_h = intval(($original_h/$original_w)*$max);
                    } else {
                        $muestra_w = intval(($original_w/$original_h)*$max);
                        $muestra_h = $max;
                    }

                    $muestra = imagecreatetruecolor($muestra_w,$muestra_h);//Creamos el molde de la imagen con las dimensiones redimensionadas
                    imagecopyresampled($muestra,$original,0,0,0,0, $muestra_w,$muestra_h,$original_w,$original_h);//Copiamos de la imagen original a la muestra
                    imagedestroy($original);//Se destruye la imagen original de la memoria.
                    imagejpeg($muestra,'../'.$path,'100');//Se crea la imagen
                }else{
                    $path = $this->path . 'invalid.jpg';//Si la imagen es invalida se una una imagen por extension invalida.
                }

                $this->images[] = $path;//Se cuarda la imagen en el array del obj.
            }
        }
        public function getImage(){
            return $this->images;//Retornamos el array del objeto.
        }
        public function deleteImg($url){
            unlink('../'.$url);
            return true;
        }
    }

?>